import React from "react";
import ReactDOM from "react-dom";
//import App from "./components/App";

import Home from "./components/Home";
import AccountSettings from "./components/AccountSettings";
import Navigation from "./components/Navigation";
import Reports from "./components/Reports";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

ReactDOM.render(( // Use ReactDOM (NOT REACT) to add the following JSX to the Document Object Model (DOM)
    // create a react router BrowserRouter, then Define a route for root (/) and display app inside this route
    <Router>
        <div>
            <Navigation />
            <Switch>
                <Route exact path="/" component={ Home } />
                <Route exact path="/settings" component={ AccountSettings } />
                <Route exact path="/reports" component={ Reports }></Route>
            </Switch>
        </div>
    </Router>
), document.querySelector("#app")); // Render the above JSX to the component in index.html with the id app