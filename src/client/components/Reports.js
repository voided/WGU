import React, { Component } from "react";

import {  } from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import socketconfig from "./socketconfig.json";

import "./Reports.scss";

class Reports extends Component {
    constructor(props) {
        super(props);
        this.state = {
            suggestions: []
        };

        this.componentDidMount = this.componentDidMount.bind(this);
    }
    render() {
        return(
        <div id="reports">
            <CssBaseline />
            <p>Something or other</p>
            {this.convert(this.state.suggestions)}
        </div>
        );
    }

    componentDidMount() {
        var ws = new WebSocket(`ws://${socketconfig.express_ip}:${socketconfig.express_port}`);
        ws.addEventListener("message", (event) => {
            var data = JSON.parse(event.data);
            if(data.action == "new") {
                var suggestionss = this.state.suggestions;
                suggestionss.push(data.data);
                this.setState({suggestions: suggestionss});
            }

            if(data.action == "connect") {
                this.setState({ suggestions: data.data });
            }

            if(data.action == "update") {
                var replace = data.replace;
                var suggestionsss = this.state.suggestions;
                suggestionsss = suggestionsss.filter(suggestion => {
                    return suggestion.message.id !== replace.message.id;
                });

                suggestionsss.push(replace);

                this.setState({ suggestions: suggestionsss });
            }

            if(data.action == "remove") {
                var suggestionssss = this.state.suggestions;
                var id = data.id;
                suggestionssss = suggestionssss.filter(suggestion => {
                    return suggestion.message.id !== id;
                });

                this.setState({ suggestions: suggestionssss });
            }
        });
    }

    convert(array) {

        array.sort((a, b) => {
            return (b.stats.upvotes - b.stats.downvotes) - (a.stats.upvotes - a.stats.downvotes);
        });

        return (
            <React.Fragment>
                {array.map(suggestion => (
                    <div key={suggestion.message.id}>
                        <span><b>{ suggestion.stats.upvotes - suggestion.stats.downvotes }</b> <a target="_blank" rel="noopener noreferrer" href={ suggestion.message.url }>{suggestion.message.content}</a></span>
                    </div>
                ))}
            </React.Fragment>
        );
    }
}

export default Reports;