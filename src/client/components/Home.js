import React, { Component } from "react";
import axios from "axios";

import {  } from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";

import "./Home.scss";

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            botstatus: ""
        };
    }
    render() {
        return(
            <div id="home">
                <CssBaseline />
                <h1>Something or other</h1>
                {/* <p>{ this.state.botstatus.toString() }</p> */}
            </div>
        );
    }

    componentDidMount() {
        axios.get("/api/botstatus")
            .then(res => {
                this.setState({
                    botstatus: res.data.tag
                });
            })
            .catch(() => {
                this.setState({
                    botstatus: null
                });
            });
    }
}

export default Home;