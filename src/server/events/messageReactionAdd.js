import WebSocket from "ws";

import config from "../../config.json";

import Suggestion from "../database/models/Suggestion";

export default async (client, wss, reaction, user) => {
    var message = reaction.message;
    var emoji = reaction.emoji;
    var author = message.author;
    var guild = message.guild;

    var member = guild.members.find("id", user.id);

    var reacted = reaction.me;

    if(user === client.user) return;

    if(emoji.name === "➕" && !reacted && (user === author || member.roles.find("name", config.staff_role))) {
        message.react("➕");
        var dataObj = {
            message: {
                id: message.id,
                content: message.cleanContent,
                url: message.url
            },
            author: {
                username: author.username,
                discriminator: author.discriminator,
                id: author.id
            },
            stats: {
                upvotes: 0,
                downvotes: 0,
                middlevotes: 0
            }
        };

        wss.broadcast({
            data: dataObj,
            action: "new"
        });

        const suggestion = new Suggestion(dataObj);

        suggestion.save((err, newSuggestion) => {
            if(err) console.error(err);
        });

        // Upvote
        await message.react("⬆");

        // Meh
        await message.react("↔");

        // Downvote
        await message.react("⬇");
    } else if(reaction.me) {
        switch(emoji.name) {
        case "⬆":
            Suggestion.findOneAndUpdate({ "message.id": message.id }, { $inc: {"stats.upvotes": 1} }, (err, doc) => {
                Suggestion.findOne({ "message.id": message.id }, (err, res) => {
                    wss.broadcast({ action: "update", replace: res });
                });
            });
            break;
        case "↔":
            Suggestion.findOneAndUpdate({ "message.id": message.id }, { $inc: {"stats.middlevotes": 1} }, (err, doc) => {
                Suggestion.findOne({ "message.id": message.id }, (err, res) => {
                    wss.broadcast({ action: "update", replace: res });
                });
            });
            break;
        case "⬇":
            Suggestion.findOneAndUpdate({ "message.id": message.id }, { $inc: {"stats.downvotes": 1} }, (err, doc) => {
                Suggestion.findOne({ "message.id": message.id }, (err, res) => {
                    wss.broadcast({ action: "update", replace: res });
                });
            });
            break;
        }
    }
};