import Commando from "discord.js-commando";
import config from "../config.json";

const client = new Commando.Client({
    owner: config.bot_owner
});

client.registry
.registerDefaultGroups()
.registerDefaultTypes();

const events = {
    MESSAGE_REACTION_ADD: "messageReactionAdd",
    MESSAGE_REACTION_REMOVE: "messageReactionRemove"
};

// "Raw event reaction add example" by Danktuary
// https://gist.github.com/Danktuary/27b3cef7ef6c42e2d3f5aff4779db8ba
client.on("raw", async event => {
    if(!events.hasOwnProperty(event.t)) return;

    const { d: data } = event;
    const user = client.users.get(data.user_id);
    const channel = client.channels.get(data.channel_id) || await user.createDM();

    if (channel.messages.has(data.message_id)) return;

    const message = await channel.fetchMessage(data.message_id);

    const emojiKey = (data.emoji.id) ? `${data.emoji.name}:${data.emoji.id}` : data.emoji.name;
    const reaction = message.reactions.get(emojiKey);

    client.emit(events[event.t], reaction, user);
});

client.login(config.discord_bot_token);

export default client;