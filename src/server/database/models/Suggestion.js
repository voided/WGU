import mongoose from "mongoose";

const suggestionSchema = new mongoose.Schema({
    message: {
        id: String,
        content: String,
        url: String
    },
    author: {
        username: String,
        discriminator: String,
        id: String
    },
    stats: {
        upvotes: {type: Number, default: 0},
        downvotes: {type: Number, default: 0},
        middlevotes: {type: Number, default: 0}
    }
});

export default mongoose.model("Suggestion", suggestionSchema);